

        BRONTO MODULE - README
______________________________________________________________________________

NAME:       Bronto
AUTHORS:    Julia Kulla-Mader <jkullama@gmail.com>
______________________________________________________________________________


DESCRIPTION

Adds a newsletter page at mysite.com/newsletter where users can subscribe to your Bronto newsletter. Bronto is a 3rd party email marketing software company. To use this module, you must have a Bronto account with API access. More information on Bronto can be found at http://bronto.com.


INSTALLATION

Step 1) Download latest release from http://drupal.org/project/bronto

Step 2)
  Extract the package into your 'modules' directory.


Step 3)
  Enable the bronto module.

Step 4)
  Go to "administer >> site configuration >> bronto" to configure the module. Bronto can provide you with your Bronto List ID if you do not already    know it. 

Step 5)
  Enable permissions appropriate to your site.
    - select the appropriate levels of permissions for 'access newsletter subscribe page' on the access control page

CREDITS & SUPPORT

Special thanks to DesignHammer of designhammer.com for supporting initial development of this module. 

All issues with this module should be reported via the following form:
http://drupal.org/node/add/project_issue/bronto
